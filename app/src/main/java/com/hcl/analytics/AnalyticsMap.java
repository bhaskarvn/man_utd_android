package com.hcl.analytics;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by amudha.p on 1/27/2016.
 */
public class AnalyticsMap {


    public static Map getMapView(String page, String Title) {

        Map<String, String> map = new HashMap<>();
        map.put("screen_title", page);
        map.put("ArticleTitle", Title);

        return map;
    }



    public static Map getMapEvent(String cat, String Action,String label) {

        Map<String, String> map = new HashMap<>();
        map.put("EventAction", Action);
        map.put("EventCategory", cat);
        map.put("EventLabel", label);

        return map;
    }
}
