package com.hcl.analytics;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.v7.app.NotificationCompat;

import com.hcl.manutd.R;
import com.urbanairship.UAirship;
import com.urbanairship.push.notifications.DefaultNotificationFactory;

public class MyApplication extends Application {
    private static MyApplication mInstance;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

	@Override
	public void onCreate() {
        MultiDex.install(getApplicationContext());
		super.onCreate();
		mInstance=this;

        TealiumHelper.initialize(this);

        UAirship.takeOff(this, new UAirship.OnReadyCallback() {
            @Override
            public void onAirshipReady(UAirship airship) {

                // Create a customized default notification factory
                DefaultNotificationFactory defaultNotificationFactory = new DefaultNotificationFactory(getApplicationContext());
                defaultNotificationFactory.setSmallIconId(R.drawable.images_logo);
                defaultNotificationFactory.setColor(NotificationCompat.COLOR_DEFAULT);

                // Set it
                airship.getPushManager().setNotificationFactory(defaultNotificationFactory);

                // Enable user notifications
                airship.getPushManager().setUserNotificationsEnabled(true);
            }
        });


	}

}
