package com.hcl.manutd;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.hcl.analytics.AnalyticsMap;
import com.hcl.analytics.TealiumHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by amudha.p on 1/23/2016.
 */
public class NewspushActivity extends Activity {

    @Bind(R.id.headingtvnews) TextView mHeading;
    @Bind(R.id.summarynews) TextView mSummary;
    @Bind(R.id.authornews) TextView mAuthor;
    @Bind(R.id.datenewstv) TextView mDate;
    @Bind(R.id.newsimg) ImageView mImgHeader;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_push_activity);

        ButterKnife.bind(this);
        Intent intent = getIntent();
        if(intent!=null) {
            String article_id = intent.getStringExtra("id");
            String article_name = intent.getStringExtra("title");
            new GetTaskNews().execute(AppGlobals.news_URL + article_id);

            TealiumHelper.trackView(savedInstanceState, AnalyticsMap.getMapView("News Article Notification", article_name));
        }

    }



    class GetTaskNews extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(params[0]);
                System.out.println(params[0] + "url");
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream is = urlConnection.getInputStream();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "");
                }
                String mResponseString = sb.toString();
                System.out.println("Web Service Response: " + mResponseString);

                return mResponseString;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    urlConnection.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                populatenewsUI(result);
            }
        }
    }

    private void populatenewsUI(String json) {
        {
            try {
                JSONObject object2 = new JSONObject(json);
                String title = object2.getString("title");
                String summary = object2.getString("summary");
                String createdDate = object2.getString("createdDate");
                String author = object2.getString("author");
                String thumbnailImage = object2.getString("thumbnailImage");
                String image = object2.getString("image");
                String videoPoster = object2.getString("videoPoster");

                mAuthor.setText(author);
                mHeading.setText(title);
                mSummary.setText(summary);
                try {
                    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat sdf1=new SimpleDateFormat("dd MMMM yyyy");
                    String val1= createdDate.substring(0, 10);
                    Date date=sdf.parse(val1);
                    mDate.setText(sdf1.format(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Picasso.with(this).load(image).fit().into(mImgHeader);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
