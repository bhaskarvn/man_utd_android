
/*/
 * Copyright 2015, Isaac Ben-Akiva
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 
*//**//**
 * FILE: IntentReceiver.java
 * AUTHOR: Dr. Isaac Ben-Akiva <isaac.ben-akiva@ubimobitech.com>
 * <p/>
 * CREATED ON: 18/01/2016
 *//**//**/

package com.hcl.manutd;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.urbanairship.UAirship;
import com.urbanairship.push.BaseIntentReceiver;
import com.urbanairship.push.PushMessage;


// Created by Isaac Ben-Akiva <isaac.ben-akiva@ubimobitech.com> on 18/01/2016.

public class IntentReceiver extends BaseIntentReceiver {
    private static final String TAG = "IntentReceiver";

    @Override
    protected void onChannelRegistrationSucceeded(Context context, String channelId) {
        Log.i(TAG, "Channel registration updated. Channel Id:" + channelId + ".");
      // Broadcast that the channel updated. Used to refresh the channel ID on the main activity.
        LocalBroadcastManager.getInstance(context).sendBroadcast(new Intent(HomeActivity.ACTION_UPDATE_CHANNEL));
    }

    @Override
    protected void onChannelRegistrationFailed(Context context) {
        Log.i(TAG, "Channel registration failed.");
    }

    @Override
    protected void onPushReceived(Context context, PushMessage message, int notificationId) {
        Log.i(TAG, "Received push notification. Alert: " + message.getAlert() +
                ". Notification ID: " + notificationId + "message" + message);


    }

    @Override
    protected void onBackgroundPushReceived(Context context, PushMessage message) {
        Log.i(TAG, "Received background push message: " + message);
    }

    @Override
    protected boolean onNotificationOpened(Context context, PushMessage message, int notificationId) {
        Log.i(TAG, "User clicked notification. Alert: " + message.getAlert());

        Intent launch = new Intent(UAirship.shared().getApplicationContext(), NewspushActivity.class);
        launch.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Bundle bun=message.getPushBundle();
        launch.putExtra("id", bun.getString("article_id"));
        launch.putExtra("title",message.getAlert());

        UAirship.shared().getApplicationContext().startActivity(launch);
        return true;
    }

    @Override
    protected boolean onNotificationActionOpened(Context context, PushMessage message,
                                                 int notificationId, String buttonId,
                                                 boolean isForeground) {
        Log.i(TAG, "User clicked notification button. Button ID: " + buttonId +
                " Alert: " + message.getAlert());

        // Return false to let UA handle launching the launch activity
        return false;
    }

    @Override
    protected void onNotificationDismissed(Context context, PushMessage message,
                                           int notificationId) {
        Log.i(TAG, "Notification dismissed. Alert: " + message.getAlert() +
                ". Notification ID: " + notificationId);
    }
}
