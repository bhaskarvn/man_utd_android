package com.hcl.manutd;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
// created by Amudhapriya.M
public class Utility {
	
	public static boolean isNetworkAvailable(Context context2) {
		ConnectivityManager cm = (ConnectivityManager) context2.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return netInfo != null && netInfo.isConnectedOrConnecting();
	}

}
