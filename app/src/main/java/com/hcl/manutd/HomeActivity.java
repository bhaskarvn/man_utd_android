package com.hcl.manutd;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gigya.socialize.GSObject;
import com.gigya.socialize.GSResponse;
import com.gigya.socialize.GSResponseListener;
import com.gigya.socialize.android.GSAPI;
import com.gigya.socialize.android.event.GSLoginUIListener;
import com.gigya.socialize.android.event.GSSocializeEventListener;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherAdView;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.hcl.analytics.AnalyticsMap;
import com.hcl.analytics.TealiumHelper;
import com.ooyala.sample.players.CustomConfiguredIMAPlayerActivity;
import com.squareup.picasso.Picasso;
import com.urbanairship.UAirship;
import com.urbanairship.google.PlayServicesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {
    private static final String TAG = HomeActivity.class.getSimpleName();
    private static final String GIGYA_API_KEY =
            "3_jyLQ1fLq-54n_scSQzLw7Mjyj-rKc7cilQ6fEdF87OlqAKepLVwAYBHFG19SVfmQ";

    private static final String GIGYA_API_KEY_live = "3_GcIsTK0P4KqP_PF8IB9yhY9zRW6hdYSdoSM6Nt1rpMYZy_sfG3jocVAgRSGnbaKK";

    public static String ACTION_UPDATE_CHANNEL = "com.hcl.manutd.ACTION_UPDATE_CHANNEL";

    @Bind(R.id.headingtv) TextView mHeading;
    @Bind(R.id.datetv) TextView mDate;
    @Bind(R.id.author) TextView mLike;
    @Bind(R.id.summary)TextView mSummary;
    @Bind(R.id.user_status)TextView mUserStatus;
    @Bind(R.id.ad_view) PublisherAdView mAdView;
    @Bind(R.id.backdrop) ImageView mImgHeader;
    @Bind(R.id.video_thumbnail) ImageView mVideoThumbnail;
    @Bind(R.id.toolbar) Toolbar mToolbar;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    public static String mAdRequest;
    private String mGiguaProvider;

    private String mImgurl_share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle("");

        initGigya();

        new GetTask().execute(AppGlobals.allnews_URL);

        mVideoThumbnail.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {

                TealiumHelper.trackEvent(arg0, AnalyticsMap.getMapEvent("click", "Video button", "android landingpage"));

                Intent intent = new Intent(HomeActivity.this, CustomConfiguredIMAPlayerActivity.class);
                startActivity(intent);

            }
        });

        // updated 19/01 -Amudha
        mLike.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
               JSONObject object = new JSONObject();
                try {

                    object.put("title", "testing");
                    object.put("userMessage", "I liked " + mHeading.getText().toString());
                    object.put("description", mSummary.getText().toString());
                    object.put("linkBack", "http://gigya.com");

                    JSONObject jsonObject=new JSONObject();
                    jsonObject.put("src",mImgurl_share);
                    jsonObject.put("href","http://gigya.com");
                    jsonObject.put("type","image");

                    object.put("mediaItems",jsonObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                GSObject gsObject = new GSObject();
                gsObject.put("userAction", object.toString());

                GSAPI.getInstance().sendRequest("socialize.publishUserAction", gsObject, new GSResponseListener() {
                    @Override
                    public void onGSResponse(String method, GSResponse response, Object context) {
                        System.out.println(response);

                        String mgetstatus=response.getString("errorCode","NA");
                        if(mgetstatus.equals("0")){
                            Toast.makeText(HomeActivity.this,"Shared Successfully", Toast.LENGTH_LONG).show();
                        }

                    }
                }, null);
            }
        });

        // Create an ad request. Check logcat output for the hashed device ID to
        // get test ads on a physical device. e.g.
        // "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
        PublisherAdRequest adRequest = new PublisherAdRequest.Builder().build();
        // Start loading the ad in the background.
        mAdView.loadAd(adRequest);


        // updated 19/01

        try {

            String response = new GetTask().execute(AppGlobals.video_list_URL).get();
            JSONArray array = new JSONArray(response);

            JSONObject object = array.getJSONObject(0);
            String string = object.getString("VideoList");

            JSONObject jsonObject = new JSONObject(string);
            JSONArray jsonArray = jsonObject.getJSONArray("Video");

            JSONObject object2 = jsonArray.getJSONObject(0);
            Videosmodel.setMcelumVideo(object2.getString("celumVideo"));
            Videosmodel.setMvideotitle(object2.getString("title"));

        } catch (Exception e) {
            e.printStackTrace();
        }


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_login) {
            if (GSAPI.getInstance().getSession() == null) {
                GSObject params = new GSObject();
                params.put("screenSet", "Default-RegistrationLogin");
                GSAPI.getInstance().showPluginDialog("accounts.screenSet",
                        params, null, null);

            } else {
                GSAPI.getInstance().logout();
                TealiumHelper.trackEvent(null, AnalyticsMap.getMapEvent("click", "Gigya Logout", mGiguaProvider));

            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void populateUI(String json) {
        try {

            JSONArray array = new JSONArray(json);

            JSONObject object = array.getJSONObject(0);
            String string = object.getString("NewsList");

            JSONObject jsonObject = new JSONObject(string);
            JSONArray array2 = jsonObject.getJSONArray("News");
            // get only first news
            // to do for loop for all news
            JSONObject object2 = array2.getJSONObject(0);
            String title = object2.getString("title");
            String summary = object2.getString("summary");
            String createdDate = object2.getString("createdDate");
            String author = object2.getString("author");
            String thumbnailImage = object2.getString("thumbnailImage");
            String image = object2.getString("image");
            String videoPoster = object2.getString("videoPoster");

           // mLike.setText(author);
            mHeading.setText(title);
            mSummary.setText(summary);
            mImgurl_share=image;

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMMM yyyy");
                String val1 = createdDate.substring(0, 10);
                Date date = sdf.parse(val1);
                mDate.setText(sdf1.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Picasso.with(this).load(image).fit().into(mImgHeader);
            Picasso.with(this).load(AppGlobals.ip_URL + videoPoster).fit().into(mVideoThumbnail);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TealiumHelper.trackView(null, AnalyticsMap.getMapView("Landing", mHeading.getText().toString()));

    }

    public void initGigya() {
        GSAPI.getInstance().initialize(this, GIGYA_API_KEY_live);
        GSAPI.getInstance().setSocializeEventListener(
                new GSSocializeEventListener() {
                    @Override
                    public void onLogout(Object context) {
                        Log.d(TAG, "Gigya logged out");
                        setUser(null);
                    }

                    @Override
                    public void onLogin(String provider, GSObject user,
                                        Object context) {
                        Log.d(TAG, "Gigya logged in with " + provider);
                        setUser(user);
                        mGiguaProvider=provider;
                        TealiumHelper.trackEvent(null, AnalyticsMap.getMapEvent("click", "Gigya Login", provider));


                    }

                    @Override
                    public void onConnectionRemoved(String provider,
                                                    Object context) {
                        Log.d(TAG, provider + " connection was removed");
                    }

                    @Override
                    public void onConnectionAdded(String provider,
                                                  GSObject user, Object context) {
                        Log.d(TAG, provider + " connection was added");
                    }
                });

        GSAPI.getInstance().sendRequest("socialize.getUserInfo", null,
                new GSResponseListener() {
                    @Override
                    public void onGSResponse(String method,
                                             GSResponse response, Object context) {
                        if (response.getErrorCode() == 0) {
                            setUser(response.getData());
                        } else {
                            setUser(null);
                        }
                    }
                }, null);


    }

    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (GSAPI.getInstance().handleAndroidPermissionsResult(requestCode,
                permissions, grantResults))
            return;

        // handle other permissions result here
    }

    public void login() {
        GSAPI.getInstance().showLoginUI(null, new GSLoginUIListener() {
            @Override
            public void onLoad(Object context) {
                Log.d(TAG, "Gigya loginUI was loaded");
            }

            @Override
            public void onError(GSResponse response, Object context) {
                Log.d(TAG,
                        "Gigya loginUI had an error - "
                                + response.getErrorMessage());
            }

            @Override
            public void onClose(boolean canceled, Object context) {
                Log.d(TAG, "Gigya loginUI was closed");
            }

            @Override
            public void onLogin(String provider, GSObject user, Object context) {
                Log.d(TAG, "Gigya loginUI has logged in");

            }
        }, null);
    }

    public void setUser(GSObject user) {
        if (user == null) {
            mUserStatus.setText("User is logged out");
            mAdRequest="NA";
            try {
                new GetLogout().execute(AppGlobals.get_cookie_URL_logout);
                new GetTask().execute(AppGlobals.allnews_URL);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            mUserStatus.setText("User is logged in as " + user.getString("nickname", ""));
            System.out.println(user);
            mAdRequest=user.getString("gender", "NA");

            Set<String> tags = new HashSet<String>();
            if(mAdRequest!="NA"){
                if(mAdRequest.equals("m")){
                    tags.add("male_group");
                    tags.remove("female_group");
                }else if(mAdRequest.equals("f")){
                    tags.add("female_group");
                    tags.remove("male_group");
                }
                UAirship.shared().getPushManager().setTags(tags);
                UAirship.shared().getPushManager().updateRegistration();

            }

            String uID = user.getString("UID", "");
            try {
                String response = new GetHeaders(getApplicationContext()).execute(AppGlobals.get_cookie_URL + uID + "&lgn=1&redirectUrl=http://livepoc.com/").get();
                if (response != null && response.contains("true")) {
                   new GetTaskCookie(getApplicationContext()).execute(AppGlobals.allnews_URL);
                } else {
                    new GetTask().execute(AppGlobals.allnews_URL);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    /**
     * Called when leaving the activity
     */
    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        TealiumHelper.onPause(this);
        super.onPause();
    }

    /**
     * Called when returning to the activity
     */
    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
        TealiumHelper.onResume(this);

    }

    /**
     * Called before the activity is destroyed
     */
    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();

        if (PlayServicesUtils.isGooglePlayStoreAvailable()) {
            PlayServicesUtils.handleAnyPlayServicesError(this);
        }

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Home Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.hcl.manutd/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Home Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.hcl.manutd/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    class GetTask extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(params[0]);
                System.out.println(params[0] + "url");
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream is = urlConnection.getInputStream();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "");
                }
                String mResponseString = sb.toString();
                System.out.println("Web Service Response: " + mResponseString);

                return mResponseString;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    urlConnection.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                populateUI(result);
            }
        }
    }




    class GetLogout extends AsyncTask<String, Void, String> {
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(params[0]);
                System.out.println(params[0] + "url");
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream is = urlConnection.getInputStream();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "");
                }
                String mResponseString = sb.toString();
                System.out.println("Web Service Response: " + mResponseString);

                return mResponseString;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    urlConnection.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        }
    }



    public class GetTaskCookie extends AsyncTask<String, Void, String> {
        Context context;

        public GetTaskCookie(Context con) {
            context = con;
        }

        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL(params[0]);
                System.out.println(params[0] + "url");
                urlConnection = (HttpURLConnection) url.openConnection();

                String[] cookieslist = GetHeaders.cookieNew.split("#");
                for(int index=0;index<cookieslist.length;index++){
                    urlConnection.setRequestProperty("Cookie",cookieslist[index]);
                }
                InputStream is = urlConnection.getInputStream();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while ((line = reader.readLine()) != null) {
                    // Append server response in string
                    sb.append(line + "");
                }
                String mResponseString = sb.toString();
                if (mResponseString != null) {
                    return mResponseString;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    urlConnection.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
               populateUI(result);
            }
        }

    }

}
