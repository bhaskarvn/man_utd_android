package com.hcl.manutd;

// created by Thilagam.C 18/01/2016
public class Videosmodel {
	private static String mcelumVideo;
	private static String mvideotitle;

	public static String getMcelumVideo() {
		return mcelumVideo;
	}

	public static void setMcelumVideo(String mcelumVideo) {
		Videosmodel.mcelumVideo = mcelumVideo;
	}

	public static String getMvideotitle() {
		return mvideotitle;
	}

	public static void setMvideotitle(String mvideotitle) {
		Videosmodel.mvideotitle = mvideotitle;
	}
}
