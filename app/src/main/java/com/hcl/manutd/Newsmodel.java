package com.hcl.manutd;

public class Newsmodel {
	


	private String mid;
	private String mnewsid;
	private String mtitle;
	private String mteasure;
	private String mcategory;
	private String mthumb_home;
	private String mthumb_SQ;
	private String mnews_thumb_home;
	private String mcreatedDate;
	private String mLocked;
	
	
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getMnewsid() {
		return mnewsid;
	}
	public void setMnewsid(String mnewsid) {
		this.mnewsid = mnewsid;
	}
	public String getMtitle() {
		return mtitle;
	}
	public void setMtitle(String mtitle) {
		this.mtitle = mtitle;
	}
	public String getMthumb_home() {
		return mthumb_home;
	}
	public void setMthumb_home(String mthumb_home) {
		this.mthumb_home = mthumb_home;
	}
	public String getMnews_thumb_home() {
		return mnews_thumb_home;
	}
	public void setMnews_thumb_home(String mnews_thumb_home) {
		this.mnews_thumb_home = mnews_thumb_home;
	}
	public String getMcategory() {
		return mcategory;
	}
	public void setMcategory(String mcategory) {
		this.mcategory = mcategory;
	}
	public String getMteasure() {
		return mteasure;
	}
	public void setMteasure(String mteasure) {
		this.mteasure = mteasure;
	}
	public String getMcreatedDate() {
		return mcreatedDate;
	}
	public void setMcreatedDate(String mcreatedDate) {
		this.mcreatedDate = mcreatedDate;
	}
	public String getmLocked() {
		return mLocked;
	}
	public void setmLocked(String mLocked) {
		this.mLocked = mLocked;
	}
	public String getMthumb_SQ() {
		return mthumb_SQ;
	}
	public void setMthumb_SQ(String mthumb_SQ) {
		this.mthumb_SQ = mthumb_SQ;
	}
	
	
	
	
	
}
