package com.hcl.manutd;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.CookieManager;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.os.AsyncTask;

public class GetHeaders extends AsyncTask<String, Void, String> {
	Context context;
	public static final String COOKIES_HEADER = "Set-Cookie";
	public static CookieManager msCookieManager = new CookieManager();

	public static String cookieNew = "";

	public GetHeaders(Context _context) {
		context = _context;
	}

	@Override
	protected String doInBackground(String... params) {
		URL url = null;
		URLConnection urlConnection = null;

		try {
			url = new URL(params[0]);
			System.out.println("login url"+url);
			urlConnection = url.openConnection();
			Map<String, List<String>> headerFields = urlConnection.getHeaderFields();

			List<String> cookiesHeader = headerFields.get(COOKIES_HEADER);

			if (cookiesHeader != null) {
				for (String cookie : cookiesHeader) {
					if (cookie != null)
						cookieNew = cookieNew.concat("#" + cookie);
				}
			}
			InputStream is = urlConnection.getInputStream();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(is));

			StringBuilder sb = new StringBuilder();
			String line = null;

			// Read Server Response
			while ((line = reader.readLine()) != null) {
				// Append server response in string
				sb.append(line + "");
			}
			String mResponseString = sb.toString();
			System.out.println("login api response"+mResponseString);
			if (mResponseString != null) {
				return mResponseString;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}
}