package com.ooyala.sample.players;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.hcl.analytics.AnalyticsMap;
import com.hcl.analytics.TealiumHelper;
import com.hcl.manutd.HomeActivity;
import com.hcl.manutd.R;
import com.hcl.manutd.Videosmodel;
import com.ooyala.android.OoyalaPlayer;
import com.ooyala.android.OoyalaPlayerLayout;
import com.ooyala.android.PlayerDomain;
import com.ooyala.android.imasdk.OoyalaIMAManager;
import com.ooyala.android.ui.OptimizedOoyalaPlayerLayoutController;

import java.util.Observable;
import java.util.Observer;

/**
 * This activity illustrates how to override IMA parameters in application code
 * 
 * Supported methods: imaManager.setAdUrlOverride(String)
 * imaManager.setAdTagParameters(Map<String, String>)
 */
public class CustomConfiguredIMAPlayerActivity extends Activity implements
		Observer {
	public final static String getName() {
		return "Custom Configured IMA Player";
	}

	final String TAG = this.getClass().toString();

	String EMBED = null;
	final String PCODE = "R2d3I6s06RyB712DN0_2GsQS-R-Y";
	final String DOMAIN = "http://ooyala.com";

	protected OptimizedOoyalaPlayerLayoutController playerLayoutController;
	protected OoyalaPlayer player;
	
	public String newurl="https://pubads.g.doubleclick.net/gampad/ads?sz=400x300&iu=/75212754/MuAdVideoUnit&impl=s&gdfp_req=1&env=vp&output=xml_vast2&unviewed_position_start=2&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]&cust_params=gender%3D";
   /**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.player_simple_frame_layout);


		TealiumHelper.trackView(savedInstanceState, AnalyticsMap.getMapView("Ooyala Video", Videosmodel.getMvideotitle()));
		String celumVideo = Videosmodel.getMcelumVideo();
		if (celumVideo != null)
			EMBED = celumVideo;

			// Initialize the player
		OoyalaPlayerLayout playerLayout = (OoyalaPlayerLayout) findViewById(R.id.ooyalaPlayer);
		player = new OoyalaPlayer(PCODE, new PlayerDomain(DOMAIN));
		playerLayoutController = new OptimizedOoyalaPlayerLayoutController(
				playerLayout, player);
		player.addObserver(this);

		/** DITA_START:<ph id="ima_custom"> **/

		OoyalaIMAManager imaManager = new OoyalaIMAManager(player);

		// This ad tag returns a midroll video
		imaManager.setAdUrlOverride(newurl+ HomeActivity.mAdRequest);
		// imaManager.setAdTagParameters(null);

		/** DITA_END:</ph> **/

		if (player.setEmbedCode(EMBED)) {
			player.play();
		}
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "Player Activity Stopped");
		if (player != null) {
			player.suspend();
		}
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d(TAG, "Player Activity Restarted");
		if (player != null) {
			player.resume();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

	}
	

	/**
	 * Listen to all notifications from the OoyalaPlayer
	 */
	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 == OoyalaPlayer.TIME_CHANGED_NOTIFICATION) {
			return;
		}
		
		Log.d(TAG,
				"Notification Received: " + arg1 + " - state: "
						+ player.getState());


		String val=player.getState().toString().trim();
		if(val.equals("PLAYING")||(val.equals("READY")||(val.equals("PAUSED")||(val.equals("SUSPENDED"))))){
			TealiumHelper.trackEvent(null, AnalyticsMap.getMapEvent("Video",player.getState().toString(),Videosmodel.getMvideotitle()));

		}

		 if(arg1.equals("adStarted")||(arg1.equals("adSkipped"))||(arg1.equals("adCompleted"))){
			 TealiumHelper.trackEvent(null, AnalyticsMap.getMapEvent("Video", arg1 + ":" + player.getState().toString(), Videosmodel.getMvideotitle()));

		 }


	}

}
