// Generated code from Butter Knife. Do not modify!
package com.hcl.manutd;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class HomeActivity$$ViewBinder<T extends com.hcl.manutd.HomeActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558570, "field 'mHeading'");
    target.mHeading = finder.castView(view, 2131558570, "field 'mHeading'");
    view = finder.findRequiredView(source, 2131558571, "field 'mDate'");
    target.mDate = finder.castView(view, 2131558571, "field 'mDate'");
    view = finder.findRequiredView(source, 2131558572, "field 'mLike'");
    target.mLike = finder.castView(view, 2131558572, "field 'mLike'");
    view = finder.findRequiredView(source, 2131558573, "field 'mSummary'");
    target.mSummary = finder.castView(view, 2131558573, "field 'mSummary'");
    view = finder.findRequiredView(source, 2131558569, "field 'mUserStatus'");
    target.mUserStatus = finder.castView(view, 2131558569, "field 'mUserStatus'");
    view = finder.findRequiredView(source, 2131558575, "field 'mAdView'");
    target.mAdView = finder.castView(view, 2131558575, "field 'mAdView'");
    view = finder.findRequiredView(source, 2131558560, "field 'mImgHeader'");
    target.mImgHeader = finder.castView(view, 2131558560, "field 'mImgHeader'");
    view = finder.findRequiredView(source, 2131558574, "field 'mVideoThumbnail'");
    target.mVideoThumbnail = finder.castView(view, 2131558574, "field 'mVideoThumbnail'");
    view = finder.findRequiredView(source, 2131558561, "field 'mToolbar'");
    target.mToolbar = finder.castView(view, 2131558561, "field 'mToolbar'");
  }

  @Override public void unbind(T target) {
    target.mHeading = null;
    target.mDate = null;
    target.mLike = null;
    target.mSummary = null;
    target.mUserStatus = null;
    target.mAdView = null;
    target.mImgHeader = null;
    target.mVideoThumbnail = null;
    target.mToolbar = null;
  }
}
