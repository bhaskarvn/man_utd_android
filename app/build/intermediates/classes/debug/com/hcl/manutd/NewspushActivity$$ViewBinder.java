// Generated code from Butter Knife. Do not modify!
package com.hcl.manutd;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class NewspushActivity$$ViewBinder<T extends com.hcl.manutd.NewspushActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131558602, "field 'mHeading'");
    target.mHeading = finder.castView(view, 2131558602, "field 'mHeading'");
    view = finder.findRequiredView(source, 2131558605, "field 'mSummary'");
    target.mSummary = finder.castView(view, 2131558605, "field 'mSummary'");
    view = finder.findRequiredView(source, 2131558604, "field 'mAuthor'");
    target.mAuthor = finder.castView(view, 2131558604, "field 'mAuthor'");
    view = finder.findRequiredView(source, 2131558603, "field 'mDate'");
    target.mDate = finder.castView(view, 2131558603, "field 'mDate'");
    view = finder.findRequiredView(source, 2131558601, "field 'mImgHeader'");
    target.mImgHeader = finder.castView(view, 2131558601, "field 'mImgHeader'");
  }

  @Override public void unbind(T target) {
    target.mHeading = null;
    target.mSummary = null;
    target.mAuthor = null;
    target.mDate = null;
    target.mImgHeader = null;
  }
}
